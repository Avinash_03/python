#Sequencial 

#String

name1 = "Avinash"
print(name1)                #Avinash    
print(type(name1))          #<class 'str'>

name2 = 'Avinash'           #standard way
print(name2)
print(type(name2))

name3 = """Avinash"""
print(name3)                
print(type(name3))

name4 = '''Avinash'''
print(name4)
print(type(name4))

#List
employee=[10,'kanha',1.5,20,'Ashish',2.5]
print(employee)                             #[10,'kanha',1.5,20,'Ashish',2.5]
print(type(employee))                       #<class 'list'>
employee[1]='Pratik'                        #List is muteable

#Tuple
employee2=(10,'kanha',1.5,20,'Ashish',2.5)
print(employee2)                             #[10,'kanha',1.5,20,'Ashish',2.5]
print(type(employee2))                       #<class 'tuple'>
#employee2[1]='Pratik'                        #List is unmuteable

#range
x=range(10)
print(x)                                    #range(0,10)
print(type(x))                              #<class 'range'>


