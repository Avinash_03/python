#Mainly 7 type of data type in paython
# 1) Numeric

#int
empId=10
print(empId)            #10
print(type(empId))      #<class 'int'>

jerNo = 7
print(jerNo)            #7
print(type(jerNo))      #<class 'int'>

#float
price=10.5
print(price)            #10.5
print(type(price))      #<class 'float'>

data = 75.1111111222222333333333
print(data)            #75.111111122222233(it print 15 no after .)
print(type(data))      #<class 'float'>

#complex
complexData=10+5j
print(complexData)              #(10+5j)
print(type(complexData))        #<class 'complex'>
