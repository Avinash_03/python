#Logical

x=True
y=False

print(x and y)          #False
print(x or y)           #True
print(not x)            #False

a=10
b=20


print(a and b)          #20  (True => b)
print(a or b)           #10  (True => a)
print(not a)            #False
