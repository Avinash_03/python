#5) Identity Operator

x=10
y=10

print(id(x))
print(id(y))

print(x is y)               #True
print(x is not y)           #False
